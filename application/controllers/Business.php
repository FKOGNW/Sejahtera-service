<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    require APPPATH . '/libraries/REST_Controller.php';
    use Restserver\Libraries\REST_Controller;

    class Business extends REST_Controller {

        function __construct($config = 'rest') {
            parent::__construct($config);
            $this->load->database();
        }

        function index_get(){
            $username= $this->get('username');
            $password= $this->get('kata_sandi');

            // if($username== '' && $password== ''){
                // $dbsejahtera= $this->db->get('accounts')->result();
            // }else{
                $this->db->where('username', $username);
                $this->db->where('kata_sandi', $password);
                $dbsejahtera= $this->db->get('accounts')->result();
            // }
        
            $this->response($dbsejahtera, 200);
        }

        function index_post(){
            $data= array(
                'nama_bisnis'=> $this->post('nama_bisnis'),
                'kategori'=> $this->post('kategori'),
                'alamat'=> $this->post('alamat'),
                'deskripsi'=> $this->post('deskripsi'));

            $insert= $this->db->insert('accounts', $data);
            if($insert){
                $this->response($data, 200);
            }else{
                $this->response(array('status'=> 'fail', 502));
            }
        }

        function index_put(){
            $username= $this->put('username');
            $data= array(
                'nama_bisnis'=> $this->put('nama_bisnis'),
                'kategori'=> $this->put('kategori'),
                'alamat'=> $this->put('alamat'),
                'deskripsi'=> $this->put('deskripsi')
                );

            $this->db->where('username', $username);
            $update= $this->db->update('accounts', $data);

            if($update){
                $this->response($data, 200);
            }else{
                $this->response(array('status'=>'fail', 502));
            }
        }
    }
?>