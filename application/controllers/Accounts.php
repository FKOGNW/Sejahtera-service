<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    require APPPATH . '/libraries/REST_Controller.php';
    use Restserver\Libraries\REST_Controller;

    class Accounts extends REST_Controller {

        function __construct($config = 'rest') {
            parent::__construct($config);
            $this->load->database();
        }

        function index_get(){
            $username= $this->get('username');
            $password= $this->get('kata_sandi');

            // if($username== '' && $password== ''){
                // $dbsejahtera= $this->db->get('accounts')->result();
            // }else{
                $this->db->where('username', $username);
                $this->db->where('kata_sandi', $password);
                $dbsejahtera= $this->db->get('accounts')->result();
            // }
        
            $this->response($dbsejahtera, 200);
        }

        function index_post(){
            $data= array(
                'username'=> $this->post('username'),
                'nama_lengkap'=> $this->post('nama_lengkap'),
                'email'=> $this->post('email'),
                'no_telepon'=> $this->post('no_telepon'),
                'kata_sandi'=> $this->post('kata_sandi'),
                'konfirmasi_kata_sandi'=> $this->post('konfirmasi_kata_sandi'),
                'nama_bisnis'=> $this->post('nama_bisnis'));

            $insert= $this->db->insert('accounts', $data);
            if($insert){
                $this->response($data, 200);
            }else{
                $this->response(array('status'=> 'fail', 502));
            }
        }

        function index_put(){
            $username= $this->put('username');
            $data= array(
                'nama_lengkap'=> $this->put('nama_lengkap'),
                'email'=> $this->put('email'),
                'no_telepon'=> $this->put('no_telepon'),
                'kata_sandi'=> $this->put('kata_sandi'),
                'konfirmasi_kata_sandi'=> $this->put('konfirmasi_kata_sandi'));

            $this->db->where('username', $username);
            $update= $this->db->update('accounts', $data);

            if($update){
                $this->response($data, 200);
            }else{
                $this->response(array('status'=>'fail', 502));
            }
        }
    }
?>