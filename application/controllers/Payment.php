<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    require APPPATH . '/libraries/REST_Controller.php';
    use Restserver\Libraries\REST_Controller;

    class Payment extends REST_Controller {

        function __construct($config = 'rest') {
            parent::__construct($config);
            $this->load->database();
        }

        // function index_get(){
        //     $username= $this->get('username');
        //     $password= $this->get('kata_sandi');

        //     // if($username== '' && $password== ''){
        //         // $dbsejahtera= $this->db->get('accounts')->result();
        //     // }else{
        //         $this->db->where('username', $username);
        //         $this->db->where('kata_sandi', $password);
        //         $dbsejahtera= $this->db->get('accounts')->result();
        //     // }
        
        //     $this->response($dbsejahtera, 200);
        // }

        // function index_post(){
        //     $data= array(
        //         'saldo'=> $this->post('saldo'),
        //         'status_pinjaman'=> $this->post('status_pinjaman'),
        //         'total_pinjaman'=> $this->post('total_pinjaman'),
        //         'cicilan'=> $this->post('cicilan'),
        //         'nominal_sudah_bayar'=> $this->post('nominal_sudah_bayar'),
        //         'nominal_belum_bayar'=> $this->post('nominal_belum_bayar'));

        //     $insert= $this->db->insert('accounts', $data);
        //     if($insert){
        //         $this->response($data, 200);
        //     }else{
        //         $this->response(array('status'=> 'fail', 502));
        //     }
        // }

        function index_put(){
            $username= $this->put('username');
            $data= array(
                'nominal_sudah_bayar'=> $this->put('nominal_sudah_bayar'),
                'nominal_belum_bayar'=> $this->put('nominal_belum_bayar'));

            $this->db->where('username', $username);
            $update= $this->db->update('accounts', $data);

            if($update){
                $this->response($data, 200);
            }else{
                $this->response(array('status'=>'fail', 502));
            }
        }
    }
?>