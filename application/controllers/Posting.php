<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    require APPPATH . '/libraries/REST_Controller.php';
    use Restserver\Libraries\REST_Controller;

    class Posting extends REST_Controller {

        function __construct($config = 'rest') {
            parent::__construct($config);
            $this->load->database();
        }

        function index_get(){

            // $id = $this->get('id');
            // if ($id == '') {
                $post = $this->db->get('post')->result();
            // } else {
                // $this->db->where('id', $id);
                // $id = $this->db->get('id')->result();
            // }
            $this->response($post, 200);
        }

        function index_post(){
            $data= array(
                'author'=> $this->post('author'),
                'title'=> $this->post('title'),
                'post'=> $this->post('post'),
                'date'=> $this->post('date'));

            $insert= $this->db->insert('post', $data);
            if($insert){
                $this->response($data, 200);
            }else{
                $this->response(array('status'=> 'fail', 502));
            }
        }

        // function index_put(){
        //     $username= $this->put('username');
        //     $data= array(
        //         'saldo'=> $this->put('saldo'),
        //         'status_pinjaman'=> $this->put('status_pinjaman'),
        //         'total_pinjaman'=> $this->put('total_pinjaman'),
        //         'cicilan'=> $this->put('cicilan'),
        //         'nominal_sudah_bayar'=> $this->put('nominal_sudah_bayar'),
        //         'nominal_belum_bayar'=> $this->put('nominal_belum_bayar'));

        //     $this->db->where('username', $username);
        //     $update= $this->db->update('accounts', $data);

        //     if($update){
        //         $this->response($data, 200);
        //     }else{
        //         $this->response(array('status'=>'fail', 502));
        //     }
        // }
    }
?>